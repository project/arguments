<?php

namespace Drupal\arguments\Events;

/**
 * Constants for classification of arguments.
 */
final class ArgumentsEvent {

  /**
   * Stored value for pro argument.
   *
   * @var integer
   */
  const ARG_PRO = 1;

  /**
   * Stored value for con argument.
   *
   * @var integer
   */
  const ARG_CON = 2;

  /**
   * The default value for argument type.
   *
   * @var integer
   */
  const ARG_DEFAULT = self::ARG_PRO;

  /**
   * Check if voting API has votes or not.
   *
   * @param int $aid
   *   Argument id to check.
   *
   * @return bool
   *   If evaluation has begun or not.
   */
  public static function hasEvaluationBegun($aid) {
    unset($aid);
    // @todo insert check for voting API.
    return FALSE;
  }

}
