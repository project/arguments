<?php

namespace Drupal\arguments\Controller;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\arguments\Entity\ArgumentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ArgumentController.
 *
 *  Returns responses for Argument routes.
 */
class ArgumentController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Argument  revision.
   *
   * @param int $argument_revision
   *   The Argument  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($argument_revision) {
    $argument = $this->entityTypeManager()->getStorage('argument')
      ->loadRevision($argument_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('argument');

    return $view_builder->view($argument);
  }

  /**
   * Page title callback for a Argument  revision.
   *
   * @param int $argument_revision
   *   The Arguments revision ID.
   *
   * @return string
   *   The page title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($argument_revision) {
    $argument = $this->entityTypeManager()->getStorage('argument')->loadRevision($argument_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $argument->label(),
      '%date' => $this->dateFormatter->format($argument->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Argument .
   *
   * @param \Drupal\arguments\Entity\ArgumentInterface $argument
   *   A Argument  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(ArgumentInterface $argument) {
    $account = $this->currentUser();
    $langcode = $argument->language()->getId();
    $langname = $argument->language()->getName();
    $languages = $argument->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $argument_storage = $this->entityTypeManager()->getStorage('argument');

    $build['#title'] = $has_translations ?
      $this->t('@langname revisions for %title', [
        '@langname' => $langname,
        '%title' => $argument->label(),
      ]) : $this->t('Revisions for %title', ['%title' => $argument->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all argument revisions") || $account->hasPermission('administer argument entities')));
    $delete_permission = (($account->hasPermission("delete all argument revisions") || $account->hasPermission('administer argument entities')));

    $rows = [];

    $vids = $argument_storage->revisionIds($argument);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\arguments\Entity\ArgumentInterface $revision */
      $revision = $argument_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $argument->getRevisionId()) {
          $link = Link::fromTextAndUrl(
            $date,
            new Url('entity.argument.revision', [
              'argument' => $argument->id(),
              'argument_revision' => $vid,
            ])
          )->toString();
        }
        else {
          $link = $argument->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.argument.translation_revert', [
                'argument' => $argument->id(),
                'argument_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.argument.revision_revert', [
                'argument' => $argument->id(),
                'argument_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.argument.revision_delete', [
                'argument' => $argument->id(),
                'argument_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['argument_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
