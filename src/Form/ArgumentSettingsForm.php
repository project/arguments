<?php

namespace Drupal\arguments\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the module settings form.
 *
 * @ingroup arguments
 */
class ArgumentSettingsForm extends ConfigFormBase {

  /**
   * The Drupal entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  public $entityTypeManager;

  /**
   * Config Manager.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entity_type_manager, CacheBackendInterface $cache_render) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheRender = $cache_render;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'arguments.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'arguments';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('arguments.settings');
    $form['max_argument_name_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Max. Argument name length'),
      '#description' => $this->t('The max length of the title heading the Argument.'),
      '#default_value' => $config->get('arguments.max_argument_name_length'),
    ];
    $form['max_argument_text_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Max. argument text length.'),
      '#description' => $this->t('The max length of the argument text body.'),
      '#default_value' => $config->get('arguments.max_argument_text_length'),
    ];

    $node_types = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $node_type) {
      $node_types[$node_type->id()] = $node_type->label();
    }

    $form['arguments_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('RulesFinder Node Types'),
      '#description' => $this->t('Enable Node types for argumentation.'),
      '#default_value' => $config->get('arguments.arguments_node_types'),
      '#options' => $node_types,
    ];

    $form['revisions_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Revisions enabled by default'),
      '#default_value' => $config->get('arguments.revisions_default'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('arguments.settings')
      ->set('arguments.max_argument_name_length', $form_state->getValue('max_argument_name_length'))
      ->set('arguments.max_argument_text_length', $form_state->getValue('max_argument_text_length'))
      ->set('arguments.arguments_node_types', $form_state->getValue('arguments_node_types'))
      ->set('arguments.revisions_default', $form_state->getValue('revisions_default'))
      ->save();
    Cache::invalidateTags(['argument_list', 'rufi_block']);
  }

}
