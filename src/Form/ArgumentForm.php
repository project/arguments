<?php

namespace Drupal\arguments\Form;

use Drupal\arguments\Events\ArgumentsEvent;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Argument edit forms.
 *
 * @ingroup arguments
 */
class ArgumentForm extends ContentEntityForm {

  use MessengerTrait;

  /**
   * The Drupal entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The Drupal ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Returns the time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $dateTime;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    /** @var \Drupal\node\NodeInterface $reference */
    $reference = $entity->getFields()['reference_id']->entity;

    $view_builder = $this->entityTypeManager->getViewBuilder($reference->getEntityTypeId());
    $reference_view = $view_builder->view($reference, 'teaser');
    $reference_view['#weight'] = -50;

    $type = $entity->get('type')->getString();
    $typeStr = [
      ArgumentsEvent::ARG_PRO => $this->t('Pro'),
      ArgumentsEvent::ARG_CON => $this->t('Contra'),
    ];

    $title = ($this->entity->isNew())
      ? $this->t('Create a new Argument')
      : $this->t('Edit %type-Argument', ['%type' => $typeStr[$type]]);
    $form = [
      'reference' => $reference_view,
      'title' => [
        '#markup' => '<h1>' . $title->render() . '</h1>',
        '#weight' => -45,
      ],
    ];

    /** @var \Drupal\arguments\Entity\Argument $entity */
    $form += parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {

      // Output PRO/CON type as text. Not allowed to change if voting has begun.
      if (ArgumentsEvent::hasEvaluationBegun($entity->id())) {
        // Unset form element and replace by rendered field value.
        $titles = [
          ArgumentsEvent::ARG_PRO => $this->t('confirms'),
          ArgumentsEvent::ARG_CON => $this->t('counters'),
        ];
        $form['type'] = [
          '#markup' => $this->t('<h2>Argument %procon the preceding entity.</h2>', [
            '%procon' => $titles[$type],
          ]),
          '#weight' => $form['type']['#weight'],
        ];
      }

      // Default settings for revision.
      $revisions_default = $this->configFactory->get('arguments.settings')
        ->get('arguments.revisions_default');
      $allow_omit = $this->currentUser->hasPermission('allow to omit creation of new argument revisions');
      $form["revision"]["#default_value"] = $revisions_default;
      if ($revisions_default && !$allow_omit) {
        $form["revision"]["#disabled"] = TRUE;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $entity = &$this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('revision') && $form_state->getValue('revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->currentUser->id());
      $entity->setRevisionTranslationAffected(TRUE);
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Argument.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Argument.', [
          '%label' => $entity->label(),
        ]));
    }

    /** @var \Drupal\node\NodeInterface $reference */
    $reference = $entity->getFields()['reference_id']->entity;
    $options = ($entity->id())
      ? ['fragment' => 'argument_' . $entity->id()] : [];
    $form_state->setRedirect('entity.node.canonical',
      ['node' => $reference->id()], $options);

    return $status;
  }

  /**
   * {@inheritdoc}
   *
   * Button-level validation handlers are highly discouraged for entity forms,
   * as they will prevent entity validation from running. If the entity is going
   * to be saved during the form submission, this method should be manually
   * invoked from the button-level validation handler, otherwise an exception
   * will be thrown.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = parent::validateForm($form, $form_state);

    $max_text_length = $this->config('arguments.settings')
      ->get('arguments.max_argument_text_length');
    $argument = $form_state->getValue('argument');
    $argument = count($argument) ? $argument[0]['value'] : '';
    $current_length = strlen($argument);
    if ($current_length > $max_text_length) {
      $form_state->setErrorByName('argument',
        $this->t("Argument is too long. It is limited to %max-length char., your text has %cur_length char.",
          ['%max-length' => $max_text_length, '%cur_length' => $current_length]));
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   *
   * Show vertical tabs ad the end of the form.
   */
  public function showRevisionUi() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * Show vertical tabs ad the end of the form.
   */
  public function addRevisionableFormFields(array &$form) {
    parent::addRevisionableFormFields($form);

    if (isset($form['revision_log_message']) && isset($form['revision_information'])) {
      $form['revision_log_message']['#group'] = 'revision_information';
    }
  }

}
