<?php

namespace Drupal\arguments\Entity;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Argument entities.
 *
 * @ingroup arguments
 */
interface ArgumentInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the Argument name.
   *
   * @return string
   *   Name of the Argument.
   */
  public function getName();

  /**
   * Sets the Argument name.
   *
   * @param string $name
   *   The Argument name.
   *
   * @return \Drupal\arguments\Entity\ArgumentInterface
   *   The called Argument entity.
   */
  public function setName($name);

  /**
   * Gets the Argument creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Argument.
   */
  public function getCreatedTime();

  /**
   * Sets the Argument creation timestamp.
   *
   * @param int $timestamp
   *   The Argument creation timestamp.
   *
   * @return \Drupal\arguments\Entity\ArgumentInterface
   *   The called Argument entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Argument published status indicator.
   *
   * Unpublished Argument are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Argument is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Argument.
   *
   * @return \Drupal\arguments\Entity\ArgumentInterface
   *   The called Argument entity.
   */
  public function setPublished(): ArgumentInterface;

  /**
   * Gets the Argument revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Argument revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\arguments\Entity\ArgumentInterface
   *   The called Argument entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Argument revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Argument revision author.
   *
   * @param int $user_id
   *   The user ID of the revision author.
   *
   * @return \Drupal\arguments\Entity\ArgumentInterface
   *   The called Argument entity.
   */
  public function setRevisionUserId($user_id);

  /**
   * Returns type of argument (pro/con) as string.
   *
   * @return string
   *   Returns type as a string.
   */
  public function getTypeStr();

  /**
   * Returns type of argument (pro/con) as integer.
   *
   * @return int
   *   Type id defined as constant in ArgumentsEvent.
   */
  public function getType();

  /**
   * Set the type of argument (PRO: 1, CON: 2).
   *
   * @param int $type
   *   Type of argument defined as constant in ArgumentsEvent.
   *
   * @return ArgumentInterface
   *   Returns full instance.
   */
  public function setType($type);

  /**
   * Get parent entity id.
   *
   * @return int
   *   Returns the entity id of the node this argument belongs to.
   */
  public function getReferenceId();

  /**
   * Set parent entity id.
   *
   * @param int|string $reference_id
   *   The entity id of the node this argument belongs to.
   *
   * @return ArgumentInterface
   *   Instance of this.
   */
  public function setReferenceId(int|string $reference_id);

  /**
   * Get translation of an argument.
   *
   * @param string $langcode
   *   The langcode.
   *
   * @return mixed
   *   Returns translation of the argument
   */
  public function getTranslation(string $langcode);

}
