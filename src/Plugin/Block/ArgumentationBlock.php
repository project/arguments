<?php

namespace Drupal\arguments\Plugin\Block;

use Drupal\arguments\ArgumentListService;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Provides a 'ArgumentationBlock' block.
 *
 * @Block(
 *  id = "argumentation_block",
 *  admin_label = @Translation("Argumentation"),
 *  category = @Translation("RulesFinder"),
 *  context_definitions = {
 *   "node" = @ContextDefinition("entity:node", label = @Translation("Current Node"))
 *  }
 * )
 */
class ArgumentationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * The arguments list service.
   *
   * @var \Drupal\arguments\ArgumentListService
   */
  protected $argumentListService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  public $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManager $entity_type_manager,
    ConfigManager $config_manager,
    ArgumentListService $argument_list_service,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->configManager = $config_manager;
    $this->argumentListService = $argument_list_service;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.manager'),
      $container->get('arguments.argument_list_service'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['introduction' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['introduction'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Introduction'),
      '#description' => '',
      '#default_value' => $this->configuration['introduction'],
      '#maxlength' => 128,
      '#size' => 60,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['introduction'] = $form_state->getValue('introduction');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#attributes' => [
        'class' => ['argumentation-block'],
      ],
      'header' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['argumentation-header'],
        ],
        'primary_actions' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['argumentation-header--actions'],
          ],
        ],
      ],
      'content' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['argumentation-content'],
        ],
      ],
      '#cache' => [
        'tags' => ['rufi_block'],
        'contexts' => ['user.roles:authenticated'],
      ],
    ];
    $node = $this->getContextValue('node');
    $node_types = $this->configManager->getConfigFactory()
      ->get('arguments.settings')
      ->get('arguments.arguments_node_types');

    /** @var \Drupal\node\NodeInterface $node */
    if (!$node->isNew() && isset($node_types[$node->getType()]) && $node_types[$node->getType()]) {

      $reference_id = $node->id();

      if (isset($this->configuration['intro']) && $this->configuration['intro']) {
        $build['header']['argumentation_block_introduction'] = [
          '#markup' => '<div class="argumentation-header--intro">' . $this->configuration['introduction'] . '</div>',
        ];
      }

      $text = $this->t('Add Argument');

      $add_link = $this->argumentListService->getAddArgumentLink($reference_id, $text);
      if (isset($add_link['#attributes'])) {
        $add_link['#attributes']['class'] = [
          'button',
          'button--primary',
          'button--action',
        ];
        $build['header']['primary_actions']['add_link'] = $add_link;
      }

      // Sorter for arguments.
      $build['header']['argumentation_sorter'] = [
        '#markup' => '<div class="votejsr argumentation-header--sorter"
                           data-votejsr="sort"
                           data-sort-plugin="argument"
                           data-additional="ttl_res:created:changed"
                           data-context=".arguments__col"></div>',
      ];

      $build['content']['argumentation_list'] = $this->argumentListService->render($reference_id);

      // Enable history automatism to mark as new.
      if (
        $this->argumentListService->moduleHandler->moduleExists('history')
        && $this->currentUser->isAuthenticated()
      ) {
        $build['content']['#attributes']['data-history-node-id'] = $reference_id;
        /* @ToDo find solution for comment module is not enabled. */
        $build['content']['#attached']['library'][] = 'comment/drupal.comment-new-indicator';
      }
    }

    return $build;
  }

}
