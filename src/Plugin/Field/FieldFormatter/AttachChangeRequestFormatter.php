<?php

namespace Drupal\arguments\Plugin\Field\FieldFormatter;

use Drupal\change_requests\Events\ChangeRequests;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'attached_improvement_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "attach_change_requests",
 *   label = @Translation("Change requests"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class AttachChangeRequestFormatter extends FormatterBase {
  use StringTranslationTrait;

  /**
   * Returns the module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current user account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;


  /**
   * Change request.
   *
   * @var \Drupal\change_requests\Events\ChangeRequests
   */
  protected ChangeRequests $crStatusConstants;

  /**
   * The template renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->currentUser = $container->get('current_user');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'display_modal' => TRUE,
      'display_add_link' => TRUE,
      'show_empty_field' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['display_add_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add link for a new patch.'),
      '#description' => $this->t("If checked an add link is displayed next to the field label."),
      '#default_value' => $this->getSetting('display_add_link'),
    ];
    $form['display_modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display patch entity in modal dialog.'),
      '#description' => $this->t("If checked the linked patch entity will be load by ajax and displayed in a modal overlay."),
      '#default_value' => $this->getSetting('display_modal'),
    ];
    $form['show_empty_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display empty field.'),
      '#description' => $this->t("Display field even when it is empty. (Recommended in combination with add link.)"),
      '#default_value' => $this->getSetting('show_empty_field'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Display add link: %add.', [
      '%add' => ($this->getSetting('display_add_link')) ? $this->t('Yes') : $this->t('No'),
    ]);
    $summary[] = $this->t('Display in modal dialog: %modal.', [
      '%modal' => ($this->getSetting('display_modal')) ? $this->t('Yes') : $this->t('No'),
    ]);
    $summary[] = $this->t('Display empty field: %empty.', [
      '%empty' => ($this->getSetting('show_empty_field')) ? $this->t('Yes') : $this->t('No'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $view = parent::view($items, $langcode);

    if ($this->getSetting('display_add_link')) {
      /** @var \Drupal\arguments\Entity\Argument $argument */
      $argument = $items->getEntity();

      $destination = [
        'entity_type' => $argument->getEntityTypeId(),
        'entity_id' => $argument->id(),
        'field' => $items->getFieldDefinition()->getName(),
      ];

      $add_link = ($this->currentUser->hasPermission('add patch entities'))
        ? [
          '#type' => 'link',
          '#title' => 'add_item',
          '#url' => Url::fromRoute('entity.node.edit_form', [
            'node' => $argument->getReferenceId(),
          ],
              ['query' => ['attach_to' => implode('/', $destination)]]
          ),
          '#options' => ['attributes' => ['class' => ['rufi-icon', 'add-link']]],
        ]
        : ['#markup' => ''];

      $view['#title'] = new TranslatableMarkup('<span>@title</span> @add_link', [
        '@title' => $view['#title'] ?? 'Change Requests',
        '@add_link' => $this->renderer->render($add_link),
      ]);
    }

    if ($this->getSetting('display_modal')) {
      $view['#attached']['library'][] = 'core/drupal.dialog.ajax';
    }

    return $view;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if (!$items->isEmpty()) {
      $this->crStatusConstants = new ChangeRequests();
      foreach ($items as $item) {
        if ($item->entity) {
          $elements[] = $this->getEntityLink($item->entity);
        }
      }
    }
    elseif ($this->getSetting('show_empty_field')) {
      $elements[] = [
        '#type' => 'container',
        'content' => [
          '#markup' => new TranslatableMarkup('<span class="empty">No change requests.</span>'),
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return ($field_definition->getSetting('target_type') == 'patch');
  }

  /**
   * Get a formatted link for a change request.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The change request entity.
   *
   * @return array
   *   Returns a renderable array of a change request link.
   */
  protected function getEntityLink(EntityInterface $entity): array {

    $route = 'entity.patch.canonical';
    $attr = ($this->getSetting('display_modal'))
      ? [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => '{"width":"80%","dialogClass":"popup-dialog-class"}',
        'role' => 'article',
      ] : [];

    $meta_props = [
      '#theme' => 'rufi_chip_set',
      '#attributes' => ['class' => ['patch_props']],
    ];
    $invoke_meta = $this->moduleHandler->invokeAll('rufi_meta_patch_patch', [
      $entity, 'list-item',
    ]);

    $url = Url::fromRoute($route, ['patch' => $entity->id()]);
    return [
      '#theme' => 'change_request__list_item',
      '#label' => $entity->label(),
      '#url' => $url->toString(),
      '#status' => array_merge($meta_props, $invoke_meta),
      '#attributes' => new Attribute($attr),
    ];
  }

}
