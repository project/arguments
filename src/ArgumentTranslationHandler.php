<?php

namespace Drupal\arguments;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for argument.
 */
class ArgumentTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
