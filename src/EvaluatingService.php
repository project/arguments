<?php

namespace Drupal\arguments;

use Drupal\Core\Database\Connection;
use Drupal\arguments\Events\ArgumentsEvent;

/**
 * Support service to eval the number of arguments (e.g. for metadata).
 */
class EvaluatingService {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new EvaluatingService object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Get number of pro and con arguments for given id.
   *
   * @param mixed $id
   *   The id of the parent entity.
   *
   * @return int[]
   *   Assoc(!) array of number of arguments (Key 1 = PRO, key 2 = CON).
   */
  public function getRuleArgumentCounts(mixed $id): array {
    $query = $this->database->select('argument_field_data', 'tb');
    $query->fields('tb', ['type']);
    $query->condition('tb.reference_id', $id);
    $query->condition('tb.status', 1);
    $args = $query->execute()->fetchAll();
    $collector = [
      ArgumentsEvent::ARG_PRO => 0,
      ArgumentsEvent::ARG_CON => 0,
    ];
    foreach ($args as $arg) {
      $collector[$arg->type]++;
    }
    return [
      'pro' => $collector[ArgumentsEvent::ARG_PRO],
      'con' => $collector[ArgumentsEvent::ARG_CON],
    ];
  }

}
